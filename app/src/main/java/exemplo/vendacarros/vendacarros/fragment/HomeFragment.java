package exemplo.vendacarros.vendacarros.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import exemplo.vendacarros.vendacarros.model.Carros;
import exemplo.vendacarros.vendacarros.R;
import exemplo.vendacarros.vendacarros.helper.RecyclerItemClickListener;
import exemplo.vendacarros.vendacarros.activity.PerfilActivity;
import exemplo.vendacarros.vendacarros.adapter.HomeAdapter;
import exemplo.vendacarros.vendacarros.api.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeFragment extends Fragment {
    private Retrofit retrofit;
    private HomeAdapter homeAdapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    public HomeFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewCarros);
        progressBar = view.findViewById(R.id.progressBarHome);

        homeAdapter = new HomeAdapter(getActivity());
        recyclerView.setAdapter(homeAdapter);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        retrofit = new Retrofit.Builder()
                .baseUrl("http://desafiobrq.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        obterDados();
        progressBar.setVisibility(View.VISIBLE);
        return view;
    }

    ArrayList<Carros> listaCarros = new ArrayList<>();
    public void obterDados() {

        ApiService service = retrofit.create(ApiService.class);
        Call<List<Carros>> listaCarrosCall = service.obterCarros();

        listaCarrosCall.enqueue(new Callback<List<Carros>>() {
            @Override
            public void onResponse(Call<List<Carros>> call, Response<List<Carros>> response) {
                if (response.isSuccessful()) {
                    List<Carros> carros = response.body();

                    listaCarros.addAll(carros);

                    recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Carros c = listaCarros.get(position);
                            int aInt = Integer.parseInt(String.valueOf(c.getId()));

                            if(aInt == 6) {
                                Toast.makeText(getActivity().getApplicationContext(), "Essa compra não está disponível", Toast.LENGTH_LONG).show();
                            } else {
                                Intent i = new Intent(getActivity(), PerfilActivity.class);
                                i.putExtra("ID", aInt);

                                startActivity(i);
                            }
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {

                        }

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        }
                    }));

                    homeAdapter.adicionarItem(listaCarros);
                    progressBar.setVisibility(View.GONE);

                } else {
                    Log.e("INFO", "onResponse" + response.errorBody());
                    Toast.makeText(getActivity(), "Erro ao conectar ao servidor.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Carros>> call, Throwable t) {
                Log.e("INFO", "onFailure" + t.getMessage());
            }
        });

    }

}
