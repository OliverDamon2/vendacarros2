package exemplo.vendacarros.vendacarros.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import exemplo.vendacarros.vendacarros.model.Carros;
import exemplo.vendacarros.vendacarros.helper.CompraDAO;
import exemplo.vendacarros.vendacarros.R;
import exemplo.vendacarros.vendacarros.helper.RecyclerItemClickListener;
import exemplo.vendacarros.vendacarros.activity.PerfilActivity;
import exemplo.vendacarros.vendacarros.adapter.CompraAdapter;

public class ComprasFragment extends Fragment {
    private RecyclerView recyclerView;
    private CompraAdapter comprasAdapter;
    private List<Carros> listaCompras = new ArrayList<>();
    private Carros carros;
    private TextView textTotal;
    private ProgressBar progressBar;
    int valor;

    public ComprasFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_compras, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewCompras);
        textTotal = view.findViewById(R.id.textTotal);
        progressBar = view.findViewById(R.id.progressBarCompras);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                carros = listaCompras.get(position);
                int aInt = Integer.parseInt(String.valueOf(carros.getId_carro()));

                Intent i = new Intent(getActivity(), PerfilActivity.class);
                i.putExtra("ID", aInt);
                startActivity(i);
            }

            @Override
            public void onLongItemClick(View view, int position) {
                removerCompra(view, position);
            }

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        }));

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalizarCompras();
            }
        });

        progressBar.setVisibility(View.VISIBLE);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        carregarCompras();
        somarCompras();
    }

    public void carregarCompras(){
        CompraDAO compraDAO = new CompraDAO(getActivity().getApplicationContext());
        listaCompras = compraDAO.listar();

        comprasAdapter = new CompraAdapter(listaCompras);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(comprasAdapter);
        progressBar.setVisibility(View.GONE);
    }

    public void somarCompras(){
        SQLiteDatabase db = getActivity().openOrCreateDatabase("DB_COMPRAS", Context.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT SUM(preco * quantidade) FROM compras;", null);
        if(c.moveToFirst()) {
            valor = c.getInt(0);
            if (valor >= 1) {
                textTotal.setText("R$: " + valor + ".0");
            } else {
                textTotal.setText("R$: " + valor);
            }
        } else {
            valor = -1;
        }
        c.close();
    }

    public void enviarCompras(){
        try {
            SQLiteDatabase db = getActivity().openOrCreateDatabase("DB_COMPRAS", Context.MODE_PRIVATE, null);
            Cursor c = db.rawQuery("SELECT * FROM compras", null);

            int nomeCarro = c.getColumnIndex("nome");
            int marcaCarro = c.getColumnIndex("marca");
            int precoCarro = c.getColumnIndex("preco");
            int quantidadeCarro = c.getColumnIndex("quantidade");
            //int imagemCarro = c.getColumnIndex("imagem");

            Intent email = new Intent(Intent.ACTION_SEND);
            email.putExtra(Intent.EXTRA_EMAIL, new String[] {"suporte@vendacarros.com"});
            email.putExtra(Intent.EXTRA_SUBJECT, "Compras");
            StringBuilder b = new StringBuilder();

            b.append("<p>Total</p>");
            b.append("<p><b>R$: "+valor+".0</b></p>");
            while (c.moveToNext()) {
                b.append("<p><b>"+c.getString(nomeCarro)+"</b></p>");
                b.append("<p><b>"+c.getString(marcaCarro)+"</b></p>");
                b.append("<p><b>R$: "+c.getString(precoCarro)+"</b></p>");
                b.append("<p><b>Quantidade: "+c.getString(quantidadeCarro)+"</b></p>");
                b.append("<b></b>");
                //b.append("<p><b>"+c.getString(imagemCarro)+"</b></p>");
            }

            email.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(b.toString()));
            email.setType("message/rfc822");
            startActivity(Intent.createChooser(email, "Escolha seu App"));
            c.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removerCompra(View view, final int position) {

        carros = listaCompras.get(position);
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Remover compra");
        alertDialog.setMessage("Tem certeza que quer remover essa compra?");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                CompraDAO compraDAO = new CompraDAO(getActivity().getApplicationContext());

                if (compraDAO.deletar(carros)){
                    carregarCompras();
                    somarCompras();
                    Toast.makeText(getActivity().getApplicationContext(), "Compra excluída com sucesso.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Erro ao excluir.", Toast.LENGTH_LONG).show();
                }

            }
        });
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getActivity().getApplicationContext(), "Cancelado", Toast.LENGTH_LONG).show();
                dialogInterface.cancel();
            }
        });

        AlertDialog alert = alertDialog.create();
        alert.show();

    }

    public void finalizarCompras() {
        if (valor >= 1) {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Finalizar compra");
            alertDialog.setMessage("Tem certeza que quer finalizar a compra? Ao clicar em confirmar, sua compra será removida e enviada.");
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final CompraDAO compraDAO = new CompraDAO(getActivity().getApplicationContext());
                final Handler handler = new Handler();
                enviarCompras();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (compraDAO.deletarTodos()) {
                            carregarCompras();
                            somarCompras();
                        }
                    }
                }, 2000);
            }
            });
            alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(getActivity().getApplicationContext(), "Cancelado", Toast.LENGTH_LONG).show();
                    dialogInterface.cancel();
                }
            });

            AlertDialog alert = alertDialog.create();
            alert.show();
        } else {
            Toast.makeText(getActivity().getApplicationContext(), "Faça alguma compra antes de finalizar", Toast.LENGTH_LONG).show();
        }
    }
}
