package exemplo.vendacarros.vendacarros.activity;

import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import exemplo.vendacarros.vendacarros.fragment.ComprasFragment;
import exemplo.vendacarros.vendacarros.fragment.HomeFragment;
import exemplo.vendacarros.vendacarros.R;
import q.rorbin.badgeview.QBadgeView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.viewPager, new HomeFragment()).commit();
    }

    private void habilitarNavegacao(BottomNavigationViewEx viewEx) {
        viewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                switch (item.getItemId()) {
                    case R.id.ic_home :
                        fragmentTransaction.replace(R.id.viewPager, new HomeFragment()).commit();
                        configuraBottomNavigationView();
                        return true;
                    case R.id.ic_compras :
                        fragmentTransaction.replace(R.id.viewPager, new ComprasFragment()).commit();
                        configuraBottomNavigationView();
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        configuraBottomNavigationView();
    }

    public void configuraBottomNavigationView(){
        BottomNavigationViewEx bottomNavigationViewEx = findViewById(R.id.bottomNavigation);
        bottomNavigationViewEx.enableAnimation(true);
        bottomNavigationViewEx.enableItemShiftingMode(true);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.setTextVisibility(true);
        habilitarNavegacao(bottomNavigationViewEx);

        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        new QBadgeView(getApplicationContext())
                .setGravityOffset(59, 2, true)
                .bindTarget(bottomNavigationViewEx.getBottomNavigationItemView(1))
                .setBadgeNumber((int) getComprasCount());
    }

    public long getComprasCount() {
        SQLiteDatabase db = openOrCreateDatabase("DB_COMPRAS", MODE_PRIVATE, null);
        long count = DatabaseUtils.queryNumEntries(db, "compras");
        db.close();
        return count;
    }

}
