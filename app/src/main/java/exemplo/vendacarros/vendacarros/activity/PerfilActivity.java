package exemplo.vendacarros.vendacarros.activity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import exemplo.vendacarros.vendacarros.R;
import exemplo.vendacarros.vendacarros.model.Carros;
import exemplo.vendacarros.vendacarros.helper.CompraDAO;
import exemplo.vendacarros.vendacarros.api.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class PerfilActivity extends AppCompatActivity {
    private Retrofit retrofit;
    private TextView textNome;
    private TextView textMarca;
    private TextView textPreco;
    private TextView textDescricao;
    private TextView textTotal;
    private ImageView imageCapa;
    private ImageButton buttonSomar;
    private ImageButton buttonSubtrair;
    private Button buttonComprar;
    private ProgressBar progressBar;
    private CardView cardPerfil;
    int soma = 1;
    int valorCompras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        textNome = findViewById(R.id.textPerfilNome);
        textMarca = findViewById(R.id.textPerfilMarca);
        textPreco = findViewById(R.id.textPerfilPreco);
        textDescricao = findViewById(R.id.textPerfilDescricao);
        textTotal = findViewById(R.id.textPerfilTotal);
        imageCapa = findViewById(R.id.imagePerfilCapa);
        buttonSomar = findViewById(R.id.imageButtonSomar);
        buttonSubtrair = findViewById(R.id.imageButtonSubtrair);
        buttonComprar = findViewById(R.id.buttonComprar);
        progressBar = findViewById(R.id.progressBarPerfil);
        cardPerfil = findViewById(R.id.cardPerfil);

        retrofit = new Retrofit.Builder()
                .baseUrl("http://desafiobrq.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        int id = getIntent().getIntExtra("ID", 0);
        obterDados(id);
        progressBar.setVisibility(View.VISIBLE);
        cardPerfil.setVisibility(View.GONE);
    }

    private void obterDados(final int id) {

        ApiService service = retrofit.create(ApiService.class);
        Call<Carros> perfilResultadoCall = service.obterPerfil(id);

        perfilResultadoCall.enqueue(new Callback<Carros>() {
            @Override
            public void onResponse(Call<Carros> call, Response<Carros> response) {

                if (response.isSuccessful()) {

                    final Carros carros = response.body();

                    textNome.setText(carros.getNome());
                    textMarca.setText(carros.getMarca());
                    textPreco.setText("R$: " + carros.getPreco());
                    textDescricao.setText(carros.getDescricao());
                    if (carros.getId() == 7 || carros.getId() == 8) {
                        Picasso.get()
                                .load("https://security.ufpb.br/intrum/contents/categorias/cordofones/gualambo/sem-imagem-1.jpg/@@images/image.jpeg")
                                .into(imageCapa);
                    } else {
                        Picasso.get()
                                .load(carros.getImagem())
                                .into(imageCapa);
                    }

                    quantidadeView();

                    buttonComprar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CompraDAO compraDAO = new CompraDAO(getApplicationContext());

                            String quantidade = textTotal.getText().toString();
                            int intQuantidade = Integer.parseInt(quantidade);
                            int valorPerfil = (25000 * intQuantidade);

                            carros.setQuantidade(quantidade);
                            carros.setId_carro(id);

                            SQLiteDatabase db = openOrCreateDatabase("DB_COMPRAS", MODE_PRIVATE, null);
                            Cursor c = db.rawQuery("SELECT * FROM compras where id_carro="+id+"", null);
                            Cursor c2 = db.rawQuery("SELECT SUM(preco * quantidade) FROM compras;", null);

                            if(c.getCount()>0) {
                                Toast.makeText(getApplicationContext(), "Essa compra foi feita. Acesse suas compras para remover e adicionar novamente", Toast.LENGTH_LONG).show();
                            } else {
                                if (c2.moveToFirst()) {
                                    valorCompras = c2.getInt(0);
                                    if (valorCompras >= 100000 || valorPerfil >= 100000) {
                                        Toast.makeText(getApplicationContext(), "Você atingiu o valor máximo de compras. Remova alguma compra", Toast.LENGTH_LONG).show();
                                    } else {
                                        if (compraDAO.salvar(carros)) {
                                            Toast.makeText(getApplicationContext(), "Compra salva com sucesso.", Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Erro ao salvar", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                            }
                            c.close();
                            c2.close();
                        }

                    });

                    progressBar.setVisibility(View.GONE);
                    cardPerfil.setVisibility(View.VISIBLE);

                } else {
                    Log.e("INFO", "onResponse" + response.errorBody());
                    Toast.makeText(PerfilActivity.this, "Erro ao conectar ao servidor.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Carros> call, Throwable t) {
                Log.e("INFO", "onErro" + t.getMessage());
            }
        });

    }

    public void quantidadeView() {

        buttonSomar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soma++;
                textTotal.setText(Integer.toString(soma));
            }
        });

        buttonSubtrair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (soma >= 1) {
                    soma--;
                    textTotal.setText(Integer.toString(soma));
                } else {
                    return;
                }
            }
        });
    }
}
