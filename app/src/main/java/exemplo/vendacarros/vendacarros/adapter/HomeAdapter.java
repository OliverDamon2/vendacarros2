package exemplo.vendacarros.vendacarros.adapter;

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import exemplo.vendacarros.vendacarros.model.Carros;
import exemplo.vendacarros.vendacarros.R;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private ArrayList<Carros> dataset;
    private Context context;

    public HomeAdapter(Context context) {
        this.context = context;
        dataset = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_data, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Carros c = dataset.get(position);
        holder.textNome.setText(c.getNome());
        holder.textMarca.setText(c.getMarca());
        if (c.getDescricao().length() > 90) {
            String descricao = c.getDescricao().substring(0, 90) + "...";
            holder.textDescricao.setText(descricao);
        }
        holder.textPreco.setText("R$: " + c.getPreco());
        if (c.getId() == 7 || c.getId() == 8) {
            Picasso.get()
                    .load("https://security.ufpb.br/intrum/contents/categorias/cordofones/gualambo/sem-imagem-1.jpg/@@images/image.jpeg")
                    .into(holder.imageCapa);
        } else {
            Picasso.get()
                    .load(c.getImagem())
                    .into(holder.imageCapa);
        }

    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void adicionarItem(ArrayList<Carros> listaCarros) {
        dataset.addAll(listaCarros);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageCapa;
        private TextView textNome;
        private TextView textMarca;
        private TextView textDescricao;
        private TextView textPreco;

        public ViewHolder(View itemView) {
            super(itemView);

            imageCapa = itemView.findViewById(R.id.imageCapa);
            textNome = itemView.findViewById(R.id.textNome);
            textMarca = itemView.findViewById(R.id.textMarca);
            textDescricao = itemView.findViewById(R.id.textDescricao);
            textPreco = itemView.findViewById(R.id.textPreco);
        }
    }

}
