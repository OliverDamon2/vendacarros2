package exemplo.vendacarros.vendacarros.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import exemplo.vendacarros.vendacarros.model.Carros;
import exemplo.vendacarros.vendacarros.R;

public class CompraAdapter extends RecyclerView.Adapter<CompraAdapter.MyViewHolder> {

    private List<Carros> listaCompras;

    public CompraAdapter(List<Carros> lista) {

        this.listaCompras = lista;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_data, parent, false);

        return new MyViewHolder(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Carros c = listaCompras.get(position);
        holder.textNome.setText(c.getNome());
        holder.textMarca.setText(c.getMarca());
        holder.textPreco.setText("R$: " + c.getPreco());
        holder.textQuantidade.setText("Quantidade: " + c.getQuantidade());
        if (c.getId_carro() == 7 || c.getId_carro() == 8) {
            Picasso.get()
                    .load("https://security.ufpb.br/intrum/contents/categorias/cordofones/gualambo/sem-imagem-1.jpg/@@images/image.jpeg")
                    .into(holder.imageCapa);
        } else {
            Picasso.get()
                    .load(c.getImagem())
                    .into(holder.imageCapa);
        }
    }

    @Override
    public int getItemCount() {
        return this.listaCompras.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textNome;
        TextView textMarca;
        TextView textQuantidade;
        TextView textPreco;
        ImageView imageCapa;

        public MyViewHolder(View itemView) {
            super(itemView);

            textNome = itemView.findViewById(R.id.textNome);
            textMarca = itemView.findViewById(R.id.textMarca);
            textPreco = itemView.findViewById(R.id.textPreco);
            textQuantidade = itemView.findViewById(R.id.textDescricao);
            imageCapa = itemView.findViewById(R.id.imageCapa);

        }
    }

}
