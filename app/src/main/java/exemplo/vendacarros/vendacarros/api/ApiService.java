package exemplo.vendacarros.vendacarros.api;

import java.util.List;

import exemplo.vendacarros.vendacarros.model.Carros;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    @GET("v1/carro/")
    Call<List<Carros>> obterCarros();


    @GET("v1/carro/{id}")
    Call<Carros> obterPerfil(@Path("id") int id);
}
