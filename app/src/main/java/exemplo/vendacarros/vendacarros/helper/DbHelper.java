package exemplo.vendacarros.vendacarros.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {

    public static int VERSION = 1;
    public static String NOME_DB = "DB_COMPRAS";
    public static String TABELA_COMPRAS = "compras";

    public DbHelper(Context context) {
        super(context, NOME_DB, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE IF NOT EXISTS " + TABELA_COMPRAS
                + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " + " nome VARCHAR, marca VARCHAR, preco VARCHAR, quantidade INT(2), imagem TEXT, id_carro INT(2)); ";

        try {

            db.execSQL(sql);
            Log.i("INFO DB", "Sucesso ao criar a tabela");

        } catch (Exception e) {
            Log.i("INFO DB", "Erro ao criar a tabela" + e.getMessage());

        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String sql = "DROP TABLE IF EXISTS " + TABELA_COMPRAS + ";";

        try {

            db.execSQL(sql);
            onCreate(db);
            Log.i("INFO DB", "Sucesso ao criar a tabela");

        } catch (Exception e) {
            Log.i("INFO DB", "Erro ao criar a tabela" + e.getMessage());

        }

    }
}
