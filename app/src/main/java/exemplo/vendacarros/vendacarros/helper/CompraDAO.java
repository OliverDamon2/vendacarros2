package exemplo.vendacarros.vendacarros.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import exemplo.vendacarros.vendacarros.model.Carros;

public class CompraDAO implements ICompraDAO {

    private SQLiteDatabase escreve;
    private SQLiteDatabase le;

    public CompraDAO(Context context) {

        DbHelper db = new DbHelper(context);
        escreve = db.getWritableDatabase();
        le = db.getReadableDatabase();

    }

    @Override
    public boolean salvar(Carros carros) {

        ContentValues cv = new ContentValues();
        cv.put("nome", carros.getNome());
        cv.put("marca", carros.getMarca());
        cv.put("preco", carros.getPreco());
        cv.put("quantidade", carros.getQuantidade());
        cv.put("imagem", carros.getImagem());
        cv.put("id_carro", carros.getId_carro());

        try {
            escreve.insert(DbHelper.TABELA_COMPRAS, null, cv);
            Log.i("INFO", "Compra salva com sucesso");
        } catch (Exception e) {
            Log.e("INFO", "Erro ao salvar compra." + e.getMessage());
            return false;
        }

        return true;
    }


    @Override
    public boolean deletar(Carros carros) {

        try {
            String[] args = {carros.getId().toString()};;
            escreve.delete(DbHelper.TABELA_COMPRAS, "id=?", args);
            Log.i("INFO", "Compra excluída.");
        } catch (Exception e) {
            Log.e("INFO", "Erro ao excluir." + e.getMessage());
            return false;
        }

        return true;
    }

    @Override
    public boolean deletarTodos() {

        try {
            escreve.delete(DbHelper.TABELA_COMPRAS, null, null);
            Log.i("INFO", "Compra excluída.");
        } catch (Exception e) {
            Log.e("INFO", "Erro ao excluir." + e.getMessage());
            return false;
        }

        return true;
    }

    @Override
    public List<Carros> listar() {
        List<Carros> carros = new ArrayList<>();

        String sql = "SELECT * FROM " + DbHelper.TABELA_COMPRAS + ";";
        Cursor c = le.rawQuery(sql, null);

        while ( c.moveToNext() ) {

            Carros carros2 = new Carros();

            Long id = c.getLong( c.getColumnIndex("id") );
            String nomeCarro = c.getString( c.getColumnIndex("nome") );
            String marcaCarro = c.getString( c.getColumnIndex("marca") );
            String precoCarro = c.getString( c.getColumnIndex("preco") );
            String quantidadeCarro = c.getString( c.getColumnIndex("quantidade") );
            String imagemCarro = c.getString( c.getColumnIndex("imagem") );
            String idCarro = c.getString( c.getColumnIndex("id_carro") );

            carros2.setId( id );
            carros2.setNome(nomeCarro);
            carros2.setMarca(marcaCarro);
            carros2.setPreco(precoCarro);
            carros2.setQuantidade(quantidadeCarro);
            carros2.setImagem(imagemCarro);
            carros2.setId_carro(Integer.parseInt(idCarro));

            carros.add( carros2 );
        }

        return carros;

    }

}
