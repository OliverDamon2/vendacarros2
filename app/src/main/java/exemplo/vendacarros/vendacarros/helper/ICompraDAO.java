package exemplo.vendacarros.vendacarros.helper;


import java.util.List;

import exemplo.vendacarros.vendacarros.model.Carros;

public interface ICompraDAO {

    public boolean salvar(Carros carros);
    public boolean deletar(Carros carros);
    public boolean deletarTodos();

    public List<Carros> listar();

}
